# -*- coding: utf-8 -*-
# This spider downloads every issue of 
# Compute! Gazette from the 1980s.

import scrapy
import os.path
from gazette.items import GazetteItem

class CompgazSpider(scrapy.Spider):
    name = 'gazette'
    allowed_domains = ['archive.org']
    start_urls = ['https://archive.org/details/compute-gazette?sort=date']
	

    def parse(self, response):
      for href in response.xpath("//div[@class='C234']/div/a/@href"):
		site = response.urljoin(href.extract())
		yield scrapy.Request(site, callback = self.parse_dir_contents)
	
    def parse_dir_contents(self, response):
		for sel in response.xpath("//*[@id='wrap']/div[4]/div/div[3]/div[1]/div[7]/a/@href").extract():
		    path = response.url.split('/')[-1]
		    yield scrapy.Request(url=response.urljoin(sel),callback=self.save_pdf)

    def save_pdf(self, response):
		path = response.url.split('/')[-1]
		rootpath = "C:\\gazette\\downloads\\"+path
		print rootpath
		if os.path.isfile(rootpath):
			    print "\n***FOUND",rootpath,"SKIPPING!***"
		else:
		     self.logger.info("\nSaving PDF %s", rootpath)
		     with open(rootpath, 'wb') as f:
			    f.write(response.body)
	